import React  from 'react';
import Transform from 'utils/Transform';
import Timer from 'utils/Timer';
import { Grid } from '@mui/material';
import './index.css';

function getClassNameForNumber(n: number, lastN: number = 1) {
    return (n > 0 && lastN > 0) ? 'Timer-active-number' : 'Timer-smoke-number';
}

export default class TimerComponent extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            getTimerState: props.children.getTimerState,
        };
    }

    componentDidMount() {
        this.setState({
            timer: new Timer(this),
        });
    }

    componentWillUnmount() {
        this.state.timer.clear();
        this.setState({ timer: null });
    }

    render() {
        const { timer, getTimerState } = this.state;
        if (timer) {
            const { isRunTimer, isClearTimer } = getTimerState();
            if (isClearTimer && timer.isNeedToClear()) {
                timer.clear();
            } else {
                isRunTimer ? timer.run() : timer.pause();
            }
        }

        const time: number = timer?.getValue() || 0;

        const { h, min, sec, mills } = Transform.getTimeFromMilliseconds(time);
        if (isNaN(sec)) {
            timer.clear();
        }
        return (
            <div className='Timer'>
                <Grid container alignItems='center' justifyContent="left" className='Timer-font'>
                    <Grid item>
                        <div className={getClassNameForNumber(h, min) + ' Timer-number-cell'}>{Transform.roundNumber(h)}</div>
                    </Grid>
                    <Grid item>
                        <div className={getClassNameForNumber(h, min) + ' Timer-comma-cell'}>:</div>
                    </Grid>
                    <Grid item>
                        <div className={getClassNameForNumber(min, sec) + ' Timer-number-cell'}>{Transform.roundNumber(min)}</div>
                    </Grid>
                    <Grid item>
                        <div className={getClassNameForNumber(min, sec) + ' Timer-comma-cell'}>:</div>
                    </Grid>
                    <Grid item>
                        <div className={getClassNameForNumber(sec, mills) + ' Timer-number-cell'}>{Transform.roundNumber(sec)}</div>
                    </Grid>
                    <Grid item>
                        <div className={getClassNameForNumber(mills, 1) + ' Timer-milliseconds-font'}>{Transform.roundNumber(mills)}</div>
                    </Grid>
                </Grid>
            </div>
        );
    }
}
