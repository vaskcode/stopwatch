import React  from 'react';
import './index.css';

type ButtonProps = {
    onClick: () => any,
    id: string
}

const ButtonComponent: React.FC<ButtonProps> = ({ children, onClick, id }) => {
    return (
        <button id={id} onClick={onClick} className={'Button'}>{children}</button>
    );
}

export default ButtonComponent;
