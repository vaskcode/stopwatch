import React  from 'react';
import './index.css';

export default class HelperComponent extends React.Component<any, any> {

    render() {
        return (
            <div className='Helper'>
                <svg width="100%" height="100%" viewBox="0 0 144 150" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M69.3376 7.64716C84.0057 5.29663 97.7422 15.0776 109.621 23.9981C120.108 31.8734 127.815 42.6299 132.321 54.9462C136.496 66.3568 134.767 78.3461 133.938 90.4681C132.987 104.378 137.232 121.08 127.142 130.703C117.052 140.326 100.545 134.709 86.6293 135.588C73.9743 136.387 61.5539 139.6 49.5197 135.605C35.7595 131.036 21.0165 124.594 14.5023 111.641C7.95736 98.627 12.0052 83.0334 16.042 69.0366C19.7377 56.2222 27.6518 45.8034 36.395 35.7324C46.0921 24.5626 54.732 9.98766 69.3376 7.64716Z"/>
                    <path d="M73.8 51.97V68.53H75.06V51.97H73.8ZM74.43 73.3C74.94 73.3 75.36 72.91 75.36 72.37C75.36 71.86 74.94 71.44 74.43 71.44C73.92 71.44 73.5 71.86 73.5 72.37C73.5 72.91 73.92 73.3 74.43 73.3Z" fill="white"/>
                </svg>
                <div className='Helper-info'>
                    <p style={{ fontFamily: 'BaufraBold' }}>HOTKEYS</p>
                    <p>START/PAUSE - SPACE</p>
                    <p>CLEAR - R</p>
                </div>
            </div>
        );
    }
}
