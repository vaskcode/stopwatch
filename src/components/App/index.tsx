import React from 'react';
import Hotkeys from 'react-hot-keys';
import TimerComponent from 'components/Timer';
import ButtonComponent from 'components/Button';
import BlobComponent from 'components/Blob';
import HelperComponent from 'components/Helper';
import './index.css';
import { HOT_KEYS } from '../../constants';

export default class AppComponent extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            timer: {
                isRunTimer: false,
                isClearTimer: false,
            }
        };
    }

    render() {
        const { isRunTimer } = this.state.timer;
        return (
            <div className='App'>
                {/*<div className='AlignGrid'></div>*/}
                <BlobComponent />
                <div className='header'>STOPWATCH</div>
                <div className='content'>
                    <div className='timer-block'>
                        <TimerComponent children={{ getTimerState: this.getTimerState }}/>
                    </div>
                    <div className='button-block'>
                        <ButtonComponent id='start_pause' children={(isRunTimer) ? 'STOP' : 'START'} onClick={this.handleTimerEvent}/>
                        <ButtonComponent id='clear' children='CLEAR' onClick={this.clearTimer}/>
                    </div>
                </div>
                <div className='footer' style={this.isTouchDevice() ? { visibility: 'hidden' } : {visibility: 'inherit'}}>
                    <HelperComponent />
                </div>

                <Hotkeys keyName={HOT_KEYS.START_PAUSE} onKeyUp={this.startPauseKeyDown}/>
                <Hotkeys keyName={HOT_KEYS.CLEAR} onKeyUp={this.startClearKeyDown}/>
            </div>
        );
    }

    isTouchDevice = () => {
        return ('ontouchstart' in window) || (navigator.maxTouchPoints > 0);
    }

    private handleTimerEvent = () => {
        const { isRunTimer } = this.state.timer;
        this.setState({
            timer: {
                isRunTimer: !isRunTimer,
                isClearTimer: false
            }
        });
    }

    private clearTimer = () => {
        this.setState({
            timer: {
                isRunTimer: false,
                isClearTimer: true
            }
        });
    }

    private getTimerState = () => {
        return this.state.timer;
    }

    private startPauseKeyDown = () => {
        document.getElementById('start_pause')?.click();
    }

    private startClearKeyDown = () => {
        document.getElementById('clear')?.click();
    }
}
