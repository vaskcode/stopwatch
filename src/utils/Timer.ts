import React from 'react';

export default class Timer {

    constructor(private readonly component: React.Component<any, any>) {}

    private static INCREMENT_VALUE: number = 10;
    private interval: NodeJS.Timeout | null = null;
    private milliseconds: number = 0;

    private updateComponent(): void {
        this.component.setState({});
    }

    run(): void {
        if (!this.interval) {
            this.interval = setInterval(
                    () => {
                        this.milliseconds += Timer.INCREMENT_VALUE;
                        this.updateComponent();
                    },
                Timer.INCREMENT_VALUE
            );
        }
    }

    pause(): void {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
    }

    clear(): void {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
        if (this.milliseconds) {
            this.milliseconds = 0;
        }
    }

    getValue(): number {
        return this.milliseconds;
    }

    isNeedToClear(): boolean {
        return Boolean(this.interval || this.milliseconds);
    }
}
