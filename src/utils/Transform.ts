export default class Transform {

    static getTimeFromMilliseconds(ms: number) {
        const time = new Date(ms);
        let h = time.getUTCHours();
        let min = time.getUTCMinutes();
        let sec = time.getUTCSeconds();
        let mills = time.getUTCMilliseconds() / 10;

        return {
            h,
            min,
            sec,
            mills
        }
    }

    static roundNumber(n: number): string {
        return n >= 10 ? `${n}` : `0${n}`
    }
}
